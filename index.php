<?php

// date — Formata a data e a hora local
// http://php.net/manual/pt_BR/function.date.php

// Fuso horario
date_default_timezone_set('America/Sao_Paulo');

// Recebe o mes anterior e o proximo
if (isset($_GET['am'])) {
	$am = $_GET['am'];
} else {
	$am = date('Y-m');
}

// Formata
$timestamp = strtotime($am . '-01');
if ($timestamp === false) {
	$am = date('Y-m');
	$timestamp = strtotime($am . '-01');
}

// Dia atual
$hoje = date('Y-m-j', time());

// Titulo H3
$titulo = date('Y / m', $timestamp);

// Mes anterior e proximo ..... mktime(hora, minuto, segundo, mes, dia, ano)
$anterior = date('Y-m', strtotime('-1 month', $timestamp));
$proximo = date('Y-m', strtotime('+1 month', $timestamp));


// Numero de dias no mes
$num_dias = date('t', $timestamp);

// 0: Domingo, 1: Segunda, 2: Terça ... 6: Sábado
$str = date('w', $timestamp);


// Cria o Calendario
$semanas = array();
$semana = '';

// Adiciona as celulas .....  .= concatena
array_push($semanas, '<tr>');
array_push($semanas, str_repeat('<td></td>', $str));	// Completa as celulas ate o dia 1 com vazio


for ($day = 1; $day <= $num_dias; $day++, $str++) {
	
	$data = $am . '-' . $day;					// Concatena ano-mes-dia e coloca em $data

	if ($data == $hoje)
		$semana .= '<td class="hoje">' . $day;	// Abre a tag 'td' e inclui a classe hoje
	else
		$semana .= '<td>' . $day;				// Abre a tag 'td'
	$semana .= '</td>';							// Fecha a tag 'td'

	array_push($semanas, $semana);

	// Fim da Semana
	if ($str % 7 == 6) {
		array_push($semanas, '</tr><tr>');		// Fecha a tag 'tr' e abre outra
	}
	// Fim do Mes
	if ($day == $num_dias) {
		array_push($semanas, str_repeat('<td></td>', 6 - ($str % 7)));	// Completa as celulas restantes com vazio
	}

	$semana = '';
}

array_push($semanas, '</tr>');


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Calendario PHP</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="css/calendario.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css" />	

	<!-- Para o Ano/Mes -->
	<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">

</head>
<body>

	<div class="container calendario">
		<h3 class="ano-mes"><a href="?am=<?= $anterior ?>"><img src="img/icons8_Back_48px.png" class="seta" alt="seta-esquerda"></a><?= $titulo ?><a href="?am=<?= $proximo ?>"><img src="img/icons8_Forward_48px.png" alt="seta-direita" class="seta"></a></h3>
		
		<table class="table table-bordered">
			<thead class="thead-light">
				<tr>
					<th>D</th>
					<th>S</th>
					<th>T</th>
					<th>Q</th>
					<th>Q</th>
					<th>S</th>
					<th>S</th>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach ($semanas as $semana) {
					echo $semana;
				}
			?>
			</tbody>
		</table>
	</div>



	
</body>
</html>